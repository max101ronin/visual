﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitApp
    {
        internal class Program
        {
            static void Main()
            {
                double sum = 1000;
                int i = 0;
                int c = 0;
                int d = 0;
                double add = 0;
                while (add < 30)
                {
                    add = sum * 0.02;
                    sum += add;
                    i++;

                    if (sum > 1200 && c < 1)
                    {
                        Console.WriteLine($"Размер вклада больше 1200 рублей через {i} месяцев");
                        c++;
                    }
                    if (add >= 30 && d < 1)
                    {
                        Console.WriteLine($"Процент по вкладу будет приносить больше 30 рублей через {i} месяцев");
                        d++;
                    }
                }
                Console.ReadKey();
            }
        }
    }
